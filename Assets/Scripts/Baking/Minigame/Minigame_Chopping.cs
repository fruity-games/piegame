﻿using UnityEngine;
using UnityEngine.UI;

public class Minigame_Chopping : MinigameBase
{
    /** <summary> Number of times player has chopped ingredients. </summary> */
    int numChops = 0;

    /** <summary> Number of times player has to chop ingredients
     * before minigame is complete. </summary> */
    int numChopsNeeded;

    /** <summary> Number of times player has to chop ingredients
     * compared to the difficulty level. </summary> */
    int[] numChopsDifficulty = new int[] { 2, 5, 10, 25 };

    /** <summary> Image to show item in while it is being chopped. </summary> */
    [Tooltip("Image to show item in while it is being chopped.")]
    public Image choppingItemImage;

    /** <summary> Image to show knife with changing sprites as player adds chops. 
     * </summary> */
    [Tooltip("Image to show knife with changing sprites as player adds chops.")]
    public GUIImageCycler knifeImage;

    void Start()
    {
        InitMinigame();

        if (itemInputSlot)
        {
            itemInputSlot.onTryReceiveItem = TryReceiveItem;
            itemInputSlot.onItemReceived = OnItemReceived;
        }

        if (choppingItemImage)
        {
            choppingItemImage.color = Color.clear;
        }
    }

    private bool TryReceiveItem(GUIDnDSlot sender)
    {
        if (sender)
        {
            if (inventory.inventory.Count == 0
                && TryGetItemProcess(sender.GetSendingItem(), out var process))
            {
                //print(process.ingredientOut);
                return true;
            }
        }
        return false;
    }

    private void OnItemReceived(IngredientListItemStruct sentItem)
    {
        if (inventory.inventory.Count > 0
            && TryGetItemProcess(sentItem, out var process))
        {
            //StartMinigame(process, OnChoppingComplete);
            StartMinigame(process, null);
        }
    }

    //private void OnChoppingComplete(bool success)
    //{
    //}

    public void AddChop()
    {
        if (isActive)
        {
            numChops += 1;
            if (knifeImage)
            {
                knifeImage.CycleImage();
            }

            if (numChops >= numChopsNeeded)
            {
                FinishMinigame();
            }
        }
    }


    // Minigame overrides


    protected override void OnStarted()
    {
        // Reset values
        numChops = 0;
        numChopsNeeded = numChopsDifficulty[(int)currentItemProcessData.minigameDifficulty];

        // Consume ingredients as soon as chopping starts.
        //ConsumeIngredients();

        // Move image from dNd slot to chopping image.
        if (itemInputSlot)
        {
            itemInputSlot.InitGUI(inventory, null);
        }
        if (choppingItemImage)
        {
            choppingItemImage.color = Color.white;
            choppingItemImage.sprite = currentItemProcessData.ingredientsIn.ingredientList[0].ingredientType.icon;
        }

        if (knifeImage)
        {
            knifeImage.ResetImage();
        }
    }

    protected override void OnFinished(bool successful)
    {
        print($"finished, successful: {successful}");

        // Remove image from chopping image.
        if (choppingItemImage)
        {
            choppingItemImage.sprite = null;
            choppingItemImage.color = Color.clear;
            print("cleared item color");
        }
        if (knifeImage)
        {
            knifeImage.ResetImage();
            print("reset knife image");
        }
    }

    protected override bool WasSuccessful()
    {
        return numChops >= numChopsNeeded;
    }


    // Helpers

    public int GetNumChops() => numChops;
    public int GetNumChopsNeeded() => numChopsNeeded;
}
