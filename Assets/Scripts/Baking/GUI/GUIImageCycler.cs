﻿using UnityEngine;
using UnityEngine.UI;

public class GUIImageCycler : MonoBehaviour
{

    public Image imageToCycle;
    public Sprite[] sprites;

    int currentIdx;

    void Start()
    {
        ResetImage();
    }

    /** <summary> Change to the next image. </summary> */
    public void CycleImage()
    {
        if (imageToCycle && sprites.Length > 0)
        {
            currentIdx += 1;
            if (currentIdx >= sprites.Length)
            {
                currentIdx = 0;
            }
            imageToCycle.sprite = sprites[currentIdx];
            imageToCycle.overrideSprite = sprites[currentIdx];
        }
    }

    /** <summary> Change to the specified image, or not change if index is
     * invalid. </summary> */
    public void CycleImage(int index)
    {
        if (imageToCycle && sprites.Length > 0 
            && index >= 0 && index < sprites.Length)
        {
            currentIdx = index - 1;
            CycleImage();
        }
    }

    /** <summary> Change to the first image. </summary> */
    public void ResetImage()
    {
        currentIdx = -1;
        CycleImage();
    }
}
