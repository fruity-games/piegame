﻿using UnityEngine;

[CreateAssetMenu(fileName = "Ingr_", menuName = "PieGame/Ingredients/Ingredient Base")]
public class IngredientBase : ScriptableObject
{
    /** <summary> Display name of ingredient. </summary> */
    [Tooltip("Display name of ingredient.")]
    public string displayName;

    /** <summary> Amount of money tourists will pay for this ingredient. 
     * </summary> */
    [Tooltip("Amount of money tourists will pay for this ingredient.")]
    public int touristCost = 1;

    /** <summary> Icon of ingredient, as a 128x128px image. </summary> */
    [Tooltip("Icon of ingredient, as a 128x128px image.")]
    public Sprite icon;
}
