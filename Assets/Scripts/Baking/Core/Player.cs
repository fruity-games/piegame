﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Inventory))]
public class Player : MonoBehaviour
{
    /** <summary> Reference to inventory component belonging to player. </summary> */
    public Inventory inventory { get; private set; }

    /** <summary> Reference to baking ingredients inventory component
     * containing items used in current baking product. </summary> */
    public Inventory bakingInventory { get; private set; }

    /** <summary> Reference to baking ingredients inventory component
     * containing items used in current baking product. </summary> */
    public List<MinigameItemForPlayerListStruct> bakingProcessList { get; private set; }

    /** <summary> Reference to inventory grid GUI in canvas. </summary> */
    public GUIInventory inventoryGUI { get; private set; }

    /** <summary> Prefab for inventory grid GUI. </summary> */
    [Tooltip("Prefab for inventory grid GUI.")]
    public GUIInventory invGUIPrefab;

    /** <summary> List of ingredients to give at start of game. </summary> */
    [Tooltip("List of ingredients to give at start of game.")]
    public IngredientListStruct startIngredients;

    /** <summary> Total amount of money player has made from selling food. </summary> */
    public int totalMoney { get; private set; }

    /** <summary> Text widget to show total money in. </summary> */
    [Tooltip("Text widget to show total money in.")]
    public Text totalMoneyText;

    void Start()
    {
        InitInventory();
    }

    protected void InitInventory()
    {
        // Set inventory references.
        var foundInvs = GetComponents<Inventory>();
        if (foundInvs.Length > 0)
        {
            inventory = foundInvs[0];
        }
        if (foundInvs.Length > 1)
        {

            bakingInventory = foundInvs[1];
            bakingProcessList = new List<MinigameItemForPlayerListStruct>();
        }

        // Create inventory GUI from prefab.
        if (inventory && invGUIPrefab)
        {
            inventory.AddItem(startIngredients);

            var canvas = FindObjectOfType<Canvas>();
            if (canvas)
            {
                inventoryGUI = Instantiate(invGUIPrefab, canvas.transform);
                if (inventoryGUI)
                {
                    // Set inventory reference to player inventory.
                    inventoryGUI.invToShow = inventory;

                    // Refresh whenever items changed.
                    inventory.onItemsChanged = inventoryGUI.RefreshGUI;
                }
            }
        }
    }

    /** <summary> Add a baking process (minigame) to the list. Warning: Only 
     * add after the process has been confirmed as complete. </summary> */
    public void SaveBakingProcess(MinigameItemStruct minigameProcess, 
        MinigameBase minigamePlayed)
    {
        // Only continue if we have the second inventory component.
        if (!bakingInventory)
        {
            return;
        }

        bakingInventory.AddItem(minigameProcess.ingredientsIn);
        bakingProcessList.Add(new MinigameItemForPlayerListStruct(
            minigameProcess,
            minigamePlayed));
    }

    public void AddMoney(int amount)
    {
        totalMoney += amount;
        if (totalMoneyText)
        {
            totalMoneyText.text = $"${totalMoney}";
        }
    }


    bool hasAddedFromUpdate = false;

    void Update()
    {
        if (!hasAddedFromUpdate && inventory.inventory.Count == 0)
        {
            inventory.AddItem(startIngredients);
            hasAddedFromUpdate = true;
        }
    }
}
