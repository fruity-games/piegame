﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour
{
    /** <summary> Called when contents of inventory changed. </summary> */
    public delegate void OnItemsChanged();


    /** <summary> Dictionary of ingredient list item structs hashed by their 
     * ingredient type. </summary> */
    public Dictionary<IngredientBase, IngredientListItemStruct> inventory { get; private set; }

    public int count { get => inventory.Count; }

    /** <summary> Called when contents of inventory changed. </summary> */
    public OnItemsChanged onItemsChanged;

    public Inventory()
    {
        Clear();
    }

    void Start()
    {
        Clear();
    }

    /** <summary> Initialise new inventory. </summary> */
    public void Clear()
    {
        inventory = new Dictionary<IngredientBase, IngredientListItemStruct>();
    }

    /** <summary> Add an item to the inventory. </summary> */
    public void AddItem(IngredientListItemStruct itemsToAdd)
    {
        // Inventory will not change if no items to add.
        if (itemsToAdd.quantity == 0)
        {
            return;
        }

        // Add new quantity to exising key if possible.
        if (inventory.TryGetValue(itemsToAdd.ingredientType, out IngredientListItemStruct ingrItem))
        {
            ingrItem.quantity += itemsToAdd.quantity;

            // Remove from inventory if has invalid quantity.
            if (ingrItem.quantity <= 0)
            {
                inventory.Remove(itemsToAdd.ingredientType);
            }

            // Propogate back to dictionary
            else
            {
                inventory[itemsToAdd.ingredientType] = ingrItem;
                onItemsChanged?.Invoke();
            }
        }

        // Otherwise add new inventory item.
        else
        {
            inventory.Add(itemsToAdd.ingredientType, itemsToAdd);
            onItemsChanged?.Invoke();
        }
    }

    public void AddItem(IngredientBase itemsToAdd)
    {
        AddItem(new IngredientListItemStruct(itemsToAdd));
    }

    public void AddItem(IngredientListStruct itemsToAdd)
    {
        if (itemsToAdd.ingredientList == null)
        {
            return;
        }

        foreach (var itemToAdd in itemsToAdd.ingredientList)
        {
            AddItem(itemToAdd);
        }
    }

    /** <summary> Check whether the inventory has the specified item. </summary> */
    public bool HasItem(IngredientListItemStruct itemsToCheck)
    {
        // Check if key exists.
        if (inventory.TryGetValue(itemsToCheck.ingredientType, out IngredientListItemStruct ingrItem))
        {
            return ingrItem.quantity >= itemsToCheck.quantity;
        }
        return false;
    }

    public bool HasItem(IngredientBase itemsToCheck)
    {
        return HasItem(new IngredientListItemStruct(itemsToCheck));
    }

    public bool HasItem(IngredientListStruct itemsToCheck)
    {
        if (itemsToCheck.ingredientList == null)
        {
            return false;
        }

        // Check every item in list.
        foreach (var itemToCheck in itemsToCheck.ingredientList)
        {
            if (!HasItem(itemToCheck))
            {
                return false;
            }
        }
        return true;
    }

    /** <summary> Remove the specified item from the inventory. </summary> */
    public void RemoveItem(IngredientListItemStruct itemsToRemove)
    {
        if (itemsToRemove.quantity == 0)
        {
            return;
        }  
        itemsToRemove.quantity = -itemsToRemove.quantity;
        AddItem(itemsToRemove);
        //// Check if key exists.
        //if (inventory.TryGetValue(itemsToRemove.ingredientType, out IngredientListItemStruct ingrItem))
        //{
        //    ingrItem.quantity -= itemsToRemove.quantity;

        //    // Remove from inventory if has invalid quantity.
        //    if (ingrItem.quantity <= 0)
        //    {
        //        inventory.Remove(itemsToRemove.ingredientType);
        //    }

        //    // Propogate back to dictionary
        //    else
        //    {
        //        inventory[itemsToRemove.ingredientType] = ingrItem;
        //    }
        //}
    }

    public void RemoveItem(IngredientBase itemsToRemove)
    {
        RemoveItem(new IngredientListItemStruct(itemsToRemove));
    }

    public void RemoveItem(IngredientListStruct itemsToRemove)
    {
        if (itemsToRemove.ingredientList == null)
        {
            return;
        }

        foreach (var itemToRemove in itemsToRemove.ingredientList)
        {
            RemoveItem(itemToRemove);
        }
    }

    public void PrintContents()
    {
        foreach (var item in inventory.Values)
        {
            print($"item: {item} HasItem(item): {HasItem(item)} HasItem(item.ingredientType): {HasItem(item.ingredientType)}");
        }
    }

    /** Number of whole items in the inventory. Ignores any items that are measured
     in grams or subitems. */
    public int wholeItemCount
    {
        get
        {
            int count = 0;
            foreach (var ingrItem in inventory.Values)
            {
                if (ingrItem.quantityType == QuantityType.WholeItem)
                {
                    count += ingrItem.quantity;
                }
            }
            return count;
        }
    }

    public IngredientListStruct ToIngredientListStruct()
    {
        if (inventory.Count == 0)
        {
            return new IngredientListStruct();
        }

        var newArr = new IngredientListItemStruct[inventory.Count];

        int i = 0;
        foreach (var ingrItem in inventory.Values)
        {
            newArr[i] = ingrItem;
            i += 1;
        }

        return new IngredientListStruct(newArr);
    }
}
