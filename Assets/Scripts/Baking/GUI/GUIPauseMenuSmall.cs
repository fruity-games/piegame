﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GUIPauseMenuSmall : MonoBehaviour
{
    public void OnQuit()
    {
        SceneManager.LoadScene("MainMenuScene");
    }
}
