﻿using UnityEngine;

public class Test_Minigame_Chopping : Minigame_Chopping
{
    public MinigameItemStruct testMinigameItem;

    public void TestStart()
    {

        if (!isActive)
        {
            // Give player enough ingredients for minigame
            Debug.Log("Giving player input ingredients.");
            var pl = FindObjectOfType<Player>();
            if (pl)
            {
                pl.inventory.AddItem(testMinigameItem.ingredientsIn);
                Debug.Log($"Player inventory now contains {pl.inventory.count} items.");
            }

            Debug.Log("Starting chopping minigame.");
            if (StartMinigame(testMinigameItem, TestOnFinished))
            {
                Debug.Log("Successfully started chopping minigame.");
            }
            else
            {
                Debug.Log("Failed to start chopping minigame.");
            }
        }
        else
        {
            Debug.Log("Minigame is already started.");
        }
    }

    public void TestAddChop()
    {
        if (!isActive)
        {
            Debug.Log("Minigame has not been started.");
            TestStart();
        }

        if (isActive)
        {
            Debug.Log($"Adding chop. Now at {GetNumChops() + 1} out of {GetNumChopsNeeded()}.");
            AddChop();
        }
    }

    public void TestCancel()
    {
        if (isActive)
        {
            Debug.Log("Cancelling current minigame.");
            CancelMinigame();
        }
        else
        {
            Debug.Log("No active minigame to cancel.");
        }
    }

    public void TestOnFinished(bool successful)
    {
        Debug.Log($"Chopping minigame complete with {(successful ? "success" : "failure")}.");
        var pl = FindObjectOfType<Player>();
        if (pl)
        {
            Debug.Log($"Player inventory now contains {pl.inventory.count} items.");
        }
    }
}
