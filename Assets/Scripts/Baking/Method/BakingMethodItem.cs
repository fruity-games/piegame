﻿using UnityEngine;

[CreateAssetMenu(fileName = "MthdIt_", menuName = "PieGame/Method/Method Item")]
public class BakingMethodItem : ScriptableObject
{
    /** <summary> Short description of what happens in this stage. </summary> */
    [TextArea()]
    [Tooltip("Short description of what happens in this stage.")]
    public string synopsis;

    /** <summary> Ingredients involved at this stage </summary> */
    [Tooltip("Ingredients involved at this stage")]
    public IngredientListStruct ingredientList;

    /** <summary> Icon of ingredient, as a 128x128px image. </summary> */
    [Tooltip("Icon of ingredient, as a 128x128px image.")]
    public Sprite icon;
}
