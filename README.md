Baking game for making fruit pies.

Will be made using Unity 2019 with C#.


## Play Now
Play the latest version here:

[https://fruity-games.gitlab.io/piegame](https://fruity-games.gitlab.io/piegame)

[https://dave22153.itch.io/piegame](https://dave22153.itch.io/piegame)

## Documentation
Read the latest documentation here:
 
[https://fruity-games.gitlab.io/piegame-docs](https://fruity-games.gitlab.io/piegame-docs)
