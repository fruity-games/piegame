﻿using System.Collections.Generic;
using UnityEngine;

/** <summary> General difficulty of the minigame. </summary> */
public enum MinigameDifficulty
{
    Easy,
    Medium,
    Hard,
    Extreme
}

/** <summary> Minigame to perform during a preparation stage. </summary> */
public enum MinigameType
{
    Chopping,
    Mixing,
    Cooking,
    Decorating
}

/** <summary> How a quantity is measured. </summary> */
public enum QuantityType
{
    WholeItem,
    SubItem,
    Gram
}

/** <summary> How to be used in drag and drop operations. </summary> */
public enum DnDInteractionMode
{
    None,
    SendOnly,
    ReceiveOnly,
    SendAndReceive
}

[System.Serializable]
public struct IngredientListItemStruct
{
    /** <summary> Display name of ingredient. </summary> */
    [Tooltip("Display name of ingredient.")]
    public IngredientBase ingredientType;

    /** <summary> Number of ingredient </summary> */
    [Tooltip("Number of ingredient")]
    public int quantity;

    /** <summary> How is the quantity measured </summary> */
    [Tooltip("How is the quantity measured")]
    public QuantityType quantityType;

    public override string ToString()
    {
        return $"{quantity} {quantityType} of {ingredientType.displayName}";
    }

    public IngredientListItemStruct(IngredientBase ingredientType)
    {
        this.ingredientType = ingredientType;
        quantity = 1;
        quantityType = QuantityType.WholeItem;
    }

    public override bool Equals(object obj)
    {
        return obj is IngredientListItemStruct @struct &&
               EqualityComparer<IngredientBase>.Default.Equals(ingredientType, @struct.ingredientType) &&
               quantity == @struct.quantity &&
               quantityType == @struct.quantityType;
    }

    public bool EqualsNoQuantity(IngredientListItemStruct @struct)
    {
        return EqualityComparer<IngredientBase>.Default.Equals(ingredientType, @struct.ingredientType);
            // Don't check quantity type either.
            //&& quantityType == @struct.quantityType;
    }

    public override int GetHashCode()
    {
        var hashCode = 1529215926;
        hashCode = hashCode * -1521134295 + EqualityComparer<IngredientBase>.Default.GetHashCode(ingredientType);
        hashCode = hashCode * -1521134295 + quantity.GetHashCode();
        hashCode = hashCode * -1521134295 + quantityType.GetHashCode();
        return hashCode;
    }

    public static bool operator ==(IngredientListItemStruct left, IngredientListItemStruct right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(IngredientListItemStruct left, IngredientListItemStruct right)
    {
        return !(left == right);
    }
}

[System.Serializable]
public struct IngredientListStruct
{
    /** <summary> Array of ingredient list items. </summary> */
    [Tooltip("Array of ingredient list items.")]
    public IngredientListItemStruct[] ingredientList;

    public IngredientListStruct(IngredientListItemStruct[] ingrList = null)
    {
        ingredientList = ingrList ?? (new IngredientListItemStruct[0]);
    }

    public bool Contains(IngredientListItemStruct item)
    {
        if (ingredientList.Length > 0)
        {
            foreach (var myItem in ingredientList)
            {
                if (item == myItem)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public bool ContainsNoQuantity(IngredientListItemStruct item)
    {
        if (ingredientList.Length > 0)
        {
            foreach (var myItem in ingredientList)
            {
                if (item.EqualsNoQuantity(myItem))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public override bool Equals(object obj)
    {
        //return obj is IngredientListStruct @struct &&
        //       EqualityComparer<IngredientListItemStruct[]>.Default.Equals(ingredientList, @struct.ingredientList);

        if (obj is IngredientListStruct @struct 
            && ingredientList.Length == @struct.ingredientList.Length)
        {
            if (ingredientList.Length == 0)
            {
                return true;
            }

            for (int i = 0; i < ingredientList.Length; i++)
            {
                if (!Contains(@struct.ingredientList[i]))
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public bool EqualsNoQuantity(IngredientListStruct @struct)
    {
        if (ingredientList.Length == @struct.ingredientList.Length)
        {
            if (ingredientList.Length == 0)
            {
                return true;
            }

            for (int i = 0; i < ingredientList.Length; i++)
            {
                if (!ContainsNoQuantity(@struct.ingredientList[i]))
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public override int GetHashCode()
    {
        return -939209910 + EqualityComparer<IngredientListItemStruct[]>.Default.GetHashCode(ingredientList);
    }

    public static bool operator ==(IngredientListStruct left, IngredientListStruct right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(IngredientListStruct left, IngredientListStruct right)
    {
        return !(left == right);
    }
}

[System.Serializable]
public struct MinigameItemStruct
{
    /** <summary> Ingredients to consume during minigame. </summary> */
    [Tooltip("Ingredients to consume during minigame.")]
    public IngredientListStruct ingredientsIn;

    ///** <summary> Minigame that must be performed to create the desired result. </summary> */
    //[Tooltip("Minigame that must be performed to create the desired result.")]
    //public MinigameType minigameToPlay;

    /** <summary> Difficulty level for minigame. </summary> */
    [Tooltip("Difficulty level for minigame.")]
    public MinigameDifficulty minigameDifficulty;

    /** <summary> The ingredient that is created out of the minigame process. </summary> */
    [Tooltip("The ingredient that is created out of the minigame process.")]
    public IngredientBase ingredientOut;
}


/** WARNING: Only for use with player minigame process list. */
public struct MinigameItemForPlayerListStruct
{
    /** <summary> Ingredients to consume during minigame. </summary> */
    [Tooltip("Ingredients to consume during minigame.")]
    public IngredientListStruct ingredientsIn;

    /** <summary> Minigame that must be performed to create the desired result. </summary> */
    [Tooltip("Minigame that must be performed to create the desired result.")]
    public MinigameBase minigameToPlay;

    /** <summary> Difficulty level for minigame. </summary> */
    [Tooltip("Difficulty level for minigame.")]
    public MinigameDifficulty minigameDifficulty;

    /** <summary> The ingredient that is created out of the minigame process. </summary> */
    [Tooltip("The ingredient that is created out of the minigame process.")]
    public IngredientBase ingredientOut;

    public MinigameItemForPlayerListStruct(MinigameItemStruct minigameItemStruct, MinigameBase minigameToPlay)
    {
        ingredientsIn = minigameItemStruct.ingredientsIn;
        minigameDifficulty = minigameItemStruct.minigameDifficulty;
        ingredientOut = minigameItemStruct.ingredientOut;

        this.minigameToPlay = minigameToPlay;
    }
}
