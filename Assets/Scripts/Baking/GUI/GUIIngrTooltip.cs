﻿using UnityEngine;
using UnityEngine.UI;

public class GUIIngrTooltip : MonoBehaviour
{
    public Text displayNameText;

    IngredientBase currentIngr;

    string _targetText;

string targetText
    {
        get => _targetText;

        set
        {
            _targetText = value + " "; // Leave padding.
            if (displayNameText)
            {
                displayNameText.text = "";
                SlowRevealText();
            }
        }
    }

    public void InitGUI(IngredientBase ingredient)
    {
        currentIngr = ingredient;
        targetText = ingredient.displayName;
    }

    private void SlowRevealText()
    {
        string currentText = displayNameText.text;
        string newText = currentText + targetText[currentText.Length];
        displayNameText.text = newText;

        if (displayNameText.text.Length < targetText.Length)
        {
            Invoke("SlowRevealText", 0.01f );
        }
    }
}
