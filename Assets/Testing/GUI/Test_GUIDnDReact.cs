﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_GUIDnDReact : MonoBehaviour
{
    public GUIDnDSlot DnDSlot;

    void Start()
    {
        DnDSlot.onDragStarted += Started;
        DnDSlot.onDragFinished += Finished;
    }

    void Started()
    {
        print("Drag started");
    }
    void Finished()
    {
        print("Drag finished");
    }
}
