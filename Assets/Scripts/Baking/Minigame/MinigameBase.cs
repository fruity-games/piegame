﻿using System;
using UnityEngine;

[RequireComponent(typeof(Inventory))]
public class MinigameBase : MonoBehaviour
{
    /** <summary> Called when minigame is complete to allow further action to
     * take place. </summary> */
    public delegate void OnComplete(bool successful);

    /** <summary> Called when minigame is complete to allow further action to
     * take place. </summary> */
    private OnComplete onComplete;

    // Call onComplete delegate like this when minigame is complete
    // with a bool param saying whether it was successful or not.
    // onComplete(true)

    /** <summary> Whether minigame is currently being played. </summary> */
    protected bool isActive = false;

    /** <summary> Whether player ingredients have been consumed. </summary> */
    protected bool hasConsumedIngr = false;

    /** <summary> Item process data currently being used for active 
     * minigame. </summary> */
    protected MinigameItemStruct currentItemProcessData;

    /** <summary> List of all available minigame processes available for this 
     * minigame. </summary> */
    [Tooltip("List of all available minigame processes available for this minigame.")]
    public MinigameItemList itemProcessList;

    /** <summary> DragNDrop receiving slot to use to input ingredients for chopping.
     * </summary> */
    [Tooltip("DragNDrop receiving slot to use to input ingredients for chopping.")]
    public GUIDnDSlot itemInputSlot;

    /** <summary> Internal inventory for this minigame. Used for input and 
     * outputting ingredients. Make sure to set the reference on start. </summary> */
    public Inventory inventory { get; protected set; }

    /** <summary> Initialise components on start. Make sure children call this in
     * void Start function. </summary> */
    protected void InitMinigame()
    {
        inventory = GetComponent<Inventory>();

        if (!itemInputSlot)
        {
            itemInputSlot = GetComponentInChildren<GUIDnDSlot>();
        }

        if (itemInputSlot)
        {
            itemInputSlot.InitGUI(inventory, null);

        }
    }

    /** <summary> Start the minigame with specified options and call
     * onCompleteCallback when minigame is complete. </summary> 
     * 
     * <returns> Whether a new minigame session was started. </returns>
     */
    public bool StartMinigame(MinigameItemStruct itemProcessData,
                                      OnComplete onCompleteCallback)
    {
        // Don't start if a game is already is progess.
        if (CanStart(itemProcessData))
        {
            // Set delegate for callback
            onComplete = onCompleteCallback;

            // Store for later
            currentItemProcessData = itemProcessData;

            // Reset values
            isActive = true;
            hasConsumedIngr = false;

            // Allow children to add behaviour here.
            OnStarted();

            return true;
        }
        return false;
    }

    /** <summary> End the minigame prematurely without providing materials
     * and allow the minigame to be replayed. </summary> */
    public void CancelMinigame()
    {
        if (isActive)
        {
            isActive = false;
            onComplete?.Invoke(false);
        }
    }

    /** <summary> End the minigame by providing materials and allow
     * allow the minigame to be replayed. </summary> */
    public void FinishMinigame()
    {
        if (CanFinish())
        {
            // Allow new minigame to begin.
            isActive = false;

            var success = WasSuccessful();
            if (success)
            {
                // Consume ingredients if they haven't been consumed already
                if (!hasConsumedIngr)
                {
                    ConsumeIngredients();
                }

                // Give result to player.
                GiveIngredients();
            }

            // Allow children to add behaviour here before the callback function.
            OnFinished(success);

            // Call onComplete delegate.
            onComplete?.Invoke(success);
        }
    }

    /** <summary> Called when minigame session successfully started. </summary> */
    protected virtual void OnStarted() { }

    /** <summary> Called when minigame session finished, before onComplete
     * delegate is called. </summary> */
    protected virtual void OnFinished(bool successful) { }

    /** <summary> Whether a new minigame session may be started. </summary> 
     * 
     * <param name="itemProcessData"> Proposed new item data to start
     * minigame with. </param>
     */
    protected virtual bool CanStart(MinigameItemStruct itemProcessData)
    {
        if (!isActive)
        {
            var pl = FindObjectOfType<Player>();
            return itemProcessData.ingredientOut
                && itemProcessData.ingredientsIn.ingredientList.Length > 0;
               // && pl && pl.inventory.HasItem(itemProcessData.ingredientsIn);
        }
        return false;
    }

    /** <summary> Whether an in-progress minigame session may
     * be terminated. </summary> */
    protected virtual bool CanFinish() => isActive;

    /** <summary> Whether current minigame session ended successfully.
     * Minigames should override to determine what counts as success. </summary> */
    protected virtual bool WasSuccessful() => true;

    /** <summary> Remove ingredients from minigame internal inventory. </summary> */
    protected void ConsumeIngredients()
    {
        // Move items from internal inventory to player's second inventory.
        if (!hasConsumedIngr)
        {
            var pl = FindObjectOfType<Player>();
            if (pl)
            {
                pl.SaveBakingProcess(currentItemProcessData, this);
            }

            // Can always remove from internal inventory.
            inventory.RemoveItem(currentItemProcessData.ingredientsIn);
            hasConsumedIngr = true;
        }
    }

    /** <summary> Add minigame ingredient result to player inventory. </summary> */
    private void GiveIngredients()
    {
        // Add item to player inventory.
        var pl = FindObjectOfType<Player>();
        if (pl && currentItemProcessData.ingredientOut)
        {
            pl.inventory.AddItem(currentItemProcessData.ingredientOut);
        }
    }

    protected bool TryGetItemProcess(IngredientListStruct inItems, out MinigameItemStruct outProcess)
    {
        // Check available item processes for this minigame.
        if (itemProcessList)
        {
            foreach (var process in itemProcessList.processes)
            {
                if (process.ingredientsIn == inItems)
                {
                    outProcess = process;
                    return true;
                }
            }
        }

        outProcess = new MinigameItemStruct();
        return false;
    }

    protected bool TryGetItemProcess(IngredientListItemStruct inItems, out MinigameItemStruct outProcess)
    {
        // Create new list with inItems.
        var inItemsList = new IngredientListStruct();
        inItemsList.ingredientList = new IngredientListItemStruct[] { inItems };

        // Check available item processes for this minigame.
        if (itemProcessList)
        {
            foreach (var process in itemProcessList.processes)
            {
                if (process.ingredientsIn.Contains(inItems))
                {
                    outProcess = process;
                    return true;
                }
            }
        }

        outProcess = new MinigameItemStruct();
        return false;
        // Perform search using list.
        //return TryGetItemProcess(inItemsList, out outProcess);
    }

    protected bool TryGetItemProcess(Inventory inItems, out MinigameItemStruct outProcess, bool checkQuantities)
    {
        // Check available item processes for this minigame.
        if (itemProcessList)
        {
            var itemList = inItems.ToIngredientListStruct();
            foreach (var process in itemProcessList.processes)
            {
                if (process.ingredientsIn.EqualsNoQuantity(itemList))
                {
                    outProcess = process;
                    return true;
                }
            }
        }

        outProcess = new MinigameItemStruct();
        return false;
    }
}
