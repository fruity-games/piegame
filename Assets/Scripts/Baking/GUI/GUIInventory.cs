﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GridLayout))]
public class GUIInventory : MonoBehaviour
{
    /** <summary> Prefab for each inventory item in grid. </summary> */
    [Tooltip("Prefab for each inventory item in grid.")]
    public GUIDnDSlot invItemPrefab;

    /** <summary> Inventory to use for showing items. </summary> */
    private Inventory _invToShow;

    /** <summary> Inventory to use for showing items. </summary> */
    public Inventory invToShow
    {
        get => _invToShow;
        set
        {
            _invToShow = value;
            RefreshGUI();
        }
    }

    /** <summary> Refresh all elements shown on screen using inventory
     * referenced by invToShow property. Only call manually if items in 
     * inventory changed. </summary> */
    public void RefreshGUI()
    {
        if (invToShow && invItemPrefab)
        {
            // Remove all exising items.
            foreach (Transform item in transform)
            {
                Destroy(item.gameObject);
            }

            // Create new items in grid.
            var grid = GetComponent<GridLayoutGroup>();
            if (grid)
            {
                foreach (var ingrItem in invToShow.inventory.Keys)
                {
                    var guiItem = Instantiate(invItemPrefab, gameObject.transform);
                    guiItem.InitGUI(invToShow, ingrItem);
                }
            }
        }
    }
}
