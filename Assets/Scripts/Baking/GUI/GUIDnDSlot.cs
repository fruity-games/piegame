﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/** <summary> UI only script that can support items from an inventory being 
 * dragged from it. </summary> */
public class GUIDnDSlot : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
{
    // Delegate declarations    

    /** <summary> Called when drag has started and active drag object
     * has been initialised. </summary> */
    public delegate void OnDragStarted();

    /** <summary> Called when drag has finished and active object
     * is no longer moved by mouse movement. </summary> */
    public delegate void OnDragFinished();

    /** <summary> Called when slot is about to receive an item from
     * a dragNdrop operation. If return true, receive the item. If
     * return false, reject the item. </summary> */
    public delegate bool OnTryReceiveItem(GUIDnDSlot sender);

    /** <summary> Called when drag from this slot has finished and 
     * target slot was able to receive item successfully. </summary> */
    public delegate void OnItemSent(IngredientListItemStruct sentItem);

    /** <summary> Called when drag from sender slot has finished and 
     * this slot was able to receive item successfully. </summary> */
    public delegate void OnItemReceived(IngredientListItemStruct sentItem);


    // Core properties

    /** <summary> Inventory object being represented. </summary> */
    public Inventory inventory { get; private set; }

    /** <summary> Key for held inventory item. </summary> */
    public IngredientBase heldIngr { get; private set; }

    /** <summary> Full inventory item referenced by heldIngr key.
     * Use <see cref="GetSendingItem"/> to get what items will be sent. </summary> */
    public IngredientListItemStruct heldInvItem => inventory.inventory[heldIngr];

    /** <summary> How to be used in drag and drop operations. </summary> */
    [Tooltip("How to be used in drag and drop operations.")]
    public DnDInteractionMode interactionMode = DnDInteractionMode.SendAndReceive;

    /** <summary> Prefab to show when dragging. </summary> */
    [Tooltip("Prefab to show when dragging.")]
    public GUIDraggedIcon cursorFollowPrefab;


    // Delegates

    /** <summary> Called when drag has started and active drag object
     * has been initialised. </summary> */    
    public OnDragStarted onDragStarted;

    /** <summary> Called when drag has finished and active object
     * is no longer moved by mouse movement. </summary> */
    public OnDragFinished onDragFinished;

    /** <summary> Called when slot is about to receive an item from
     * a dragNdrop operation. If return true, receive the item. If
     * return false, reject the item. </summary> */
    public OnTryReceiveItem onTryReceiveItem;

    /** <summary> Called when drag from this slot has finished and 
     * target slot was able to receive item successfully. </summary> */
    public OnItemSent onItemSent;

    /** <summary> Called when drag from sender slot has finished and 
     * this slot was able to receive item successfully. </summary> */
    public OnItemReceived onItemReceived;


    Canvas canvas;
    GraphicRaycaster raycaster;
    GUIDraggedIcon cursorFollower;


    // GUI References

    /** <summary> Image component to show ingredient icon in. </summary> */
    [Tooltip("Image component to show ingredient icon in.")]
    public Image ingrIcon;

    /** <summary> Text object to set item quantity in. </summary> */
    [Tooltip("Text object to set item quantity in.")]
    public Text quantityText;

    public IngredientListItemStruct testItem;
    public Inventory testInv;
    // Methods

    /** <summary> Set default values. </summary> */
    void Start()
    {
        canvas = FindObjectOfType<Canvas>();
        if (canvas)
        {
            raycaster = canvas.GetComponent<GraphicRaycaster>();
        }

        // Uncomment this line causes images to disappear during BUILT game only.
        InitGUI();

        //if (testInv)
        //{
        //    inventory = testInv;
        //    if (testItem.ingredientType)
        //    {
        //        testInv.AddItem(testItem);
        //    }
        //    heldIngr = testItem.ingredientType;
        //    InitGUI();
        //}
    }

    /** <summary> Set initial GUI display values for showing inventory item. </summary> */
    public void InitGUI(Inventory inventory, IngredientBase key)
    {
        // Set references.
        this.inventory = inventory // Inventory must be provided.
            ?? throw new System.ArgumentNullException(nameof(inventory));
        heldIngr = key; // Allow null key, meaning no held item in this slot.
           // ?? throw new System.ArgumentNullException(nameof(key));

        // Set GUI display values.
        InitGUI();
    }

    /** <summary> Set GUI display values for showing inventory item. </summary> */
    public void InitGUI()
    {
        if (isFree)
        {
            if (ingrIcon)
            {
                ingrIcon.color = Color.clear;
            }
            if (quantityText)
            {
                quantityText.text = "";
            }
        }
        else
        {
            if (ingrIcon)
            {
                ingrIcon.sprite = heldIngr.icon;
                ingrIcon.color = Color.white;
            }
            if (quantityText)
            {
                quantityText.text = heldInvItem.quantity.ToString();
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (CanDrag())
        {
            // Create new drag and drop object and start it dragging.
            if (cursorFollowPrefab)
            {
                cursorFollower = Instantiate(cursorFollowPrefab, Input.mousePosition, new Quaternion(), canvas.transform);
                cursorFollower.InitGUI(this);
            }

            onDragStarted?.Invoke();
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        // Perform graphic raycast for UI elements.
        if (isDragging)
        {
            Destroy(cursorFollower.gameObject, .01f);
            EventSystem.current.SetSelectedGameObject(null);

            if (CanDrop())
            {
                var results = new List<RaycastResult>();
                raycaster.Raycast(eventData, results);

                foreach (var raycast in results)
                {
                    if (raycast.gameObject && raycast.gameObject != gameObject)
                    {
                        var dndTarget = raycast.gameObject.gameObject.GetComponent<GUIDnDSlot>();
                        if (dndTarget && dndTarget.CanReceive(GetSendingItem()))
                        {
                            GiveIngr(dndTarget);
                        }
                    }
                }
            }

            onDragFinished?.Invoke();
        }        
    }

    /** <summary> Give item held in this slot to receiving slot. </summary> */
    public void GiveIngr(GUIDnDSlot target)
    {
        var sendingItem = GetSendingItem();
        if (target && target.CanReceive(sendingItem) && CanSend())
        {
            // If willReceive is null, no delegate has been bound.
            // In this case, the default is to allow the transfer.
            var willReceive = target.onTryReceiveItem?.Invoke(this);
            if (willReceive == null || (bool)willReceive)
            {

                target.inventory.AddItem(sendingItem);
                target.heldIngr = heldIngr;

                inventory.RemoveItem(sendingItem);
                if (!inventory.HasItem(sendingItem.ingredientType))
                {
                    heldIngr = null;
                }

                onItemSent?.Invoke(sendingItem);
                target.onItemReceived?.Invoke(sendingItem);

                InitGUI();
                target.InitGUI();
            }

        }
    }

    //void Update()
    //{
    //    if (Input.GetMouseButtonUp(0) && isDragging)
    //    {
    //        Destroy(cursorFollower.gameObject, .01f);
    //        EventSystem.current.SetSelectedGameObject(null);
    //    }
    //}


    // Helpers

    /** <summary> Whether this slot may be used for sending. </summary> */
    private bool isSender => interactionMode == DnDInteractionMode.SendOnly ||
                             interactionMode == DnDInteractionMode.SendAndReceive;

    /** <summary> Whether this slot may be used for receiving. </summary> */
    private bool isReceiver => interactionMode == DnDInteractionMode.ReceiveOnly ||
                               interactionMode == DnDInteractionMode.SendAndReceive;

    private bool CanSend() => !isFree && isSender;
    private bool CanReceive(IngredientListItemStruct items) => 
        (isFree || items.ingredientType == heldIngr) && isReceiver;

    private bool CanDrag() => !isFree && isSender && raycaster;
    private bool CanDrop() => isSender && raycaster;

    public bool isFree => !(heldIngr && inventory && inventory.HasItem(heldIngr));
    private bool isDragging => cursorFollower;

    /** <summary> Item that will be sent. Only valid while has item. </summary> */
    public IngredientListItemStruct GetSendingItem()
    {
        // For now, always just return a single quantity of the item.
        var send = heldInvItem;
        send.quantity = 1;
        return send;
    }


    // Tooltip

    public GUIIngrTooltip tooltipPrefab;
    private GUIIngrTooltip tooltipWidget;

    public void OnPointerEnter(PointerEventData eventData)
    {
        Invoke("CreateTooltip", 1f);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (tooltipWidget)
        {
            Destroy(tooltipWidget.gameObject);
        }
        CancelInvoke();
    }

    private void CreateTooltip()
    {
        // Create new tooltip widget.
        if (heldIngr && canvas && tooltipPrefab)
        {
            tooltipWidget = Instantiate(tooltipPrefab, Input.mousePosition, new Quaternion(), canvas.transform);
            tooltipWidget.InitGUI(heldIngr);
        }
    }


}
