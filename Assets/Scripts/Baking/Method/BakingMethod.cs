﻿using UnityEngine;

[CreateAssetMenu(fileName = "Mthd_", menuName = "PieGame/Method/Method")]
public class BakingMethod : ScriptableObject
{
    /** <summary> Stages involved in the method. </summary> */
    [Tooltip("Stages involved in the method.")]
    public BakingMethodItem[] methodItems;
}
