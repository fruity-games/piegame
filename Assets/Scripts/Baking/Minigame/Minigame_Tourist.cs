﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Minigame_Tourist : MinigameBase
{

    public Text touristMessageText;

    string[] messages = {"Thanks", "Thank you!", "Thanks, buddy",
        "Yummy!", "Delicious", "What a surprise", "Eww... gross!",
        "Totally lame", "Nice cake, pal", "That was nice",
        "Perfect!", "That's exactly what I want", "Perfection!",
        "Very nice indeed", "LoL!"};

    bool isShowing = false;

    // Use this for initialization
    void Start()
    {
        InitMinigame();

        if (itemInputSlot)
        {
            itemInputSlot.onItemReceived = OnItemReceived;
        }
    }

    void OnItemReceived(IngredientListItemStruct sentItem)
    {
        if (touristMessageText)
        {
            isShowing = true;
            touristMessageText.color = Color.white;
            touristMessageText.text = messages[Random.Range(0, messages.Length)];

            // Add player money.
            var pl = FindObjectOfType<Player>();
            if (pl)
            {
                pl.AddMoney(sentItem.ingredientType.touristCost);
            }

            if (isShowing)
            {
                CancelInvoke();
            }
            Invoke("ResetMyComp", 3f);
        }

        inventory.Clear();
        if (itemInputSlot)
        {
            itemInputSlot.InitGUI(inventory, null);
        }
    }

    void ResetMyComp()
    {
        touristMessageText.color = Color.clear;
        isShowing = false;
    }
}
