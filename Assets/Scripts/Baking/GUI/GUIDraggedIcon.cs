﻿using UnityEngine;
using UnityEngine.UI;

/** <summary> gameObject(prefab) that can be instantiated on drag start,
 * follow mouse cursor during drag and be destroyed on drag receive. </summary> */
public class GUIDraggedIcon : MonoBehaviour
{
    public GUIDnDSlot sender { get; private set; }

    /** <summary> Image component to show ingredient icon in. </summary> */
    [Tooltip("Image component to show ingredient icon in.")]
    public Image ingrIcon;

    public void InitGUI(GUIDnDSlot sender)
    {
        this.sender = sender 
            ?? throw new System.ArgumentNullException(nameof(sender));
        if (ingrIcon)
        {
            ingrIcon.sprite = sender.heldIngr.icon;
        }
    }

    void Update()
    {
        // Update position to match mouse cursor.
        gameObject.transform.position = GetCurrentPos();
    }

    private Vector3 GetCurrentPos() => Input.mousePosition;
}
