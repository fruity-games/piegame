﻿using UnityEngine;

[CreateAssetMenu(fileName = "MngmItList_", menuName = "PieGame/Minigame/Minigame Item List")]
public class MinigameItemList : ScriptableObject
{
    /** <summary> Array of minigame items that may be performed. </summary> */
    [Tooltip("Array of minigame items that may be performed.")]
    public MinigameItemStruct[] processes;
}