﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GUIMainMenu : MonoBehaviour
{

    public GameObject creditsMenu;


    // Start is called before the first frame update
    void Start()
    {
        if (creditsMenu)
        {
            creditsMenu.SetActive(false);
        }

        foreach (var slot in FindObjectsOfType<GUIDnDSlot>())
        {
            switch (slot.gameObject.name)
            {
                default:
                    break;
                case "EnterBakerySlot":
                    slot.onTryReceiveItem = OnEnterBakerySlot;
                    break;
                case "CreditsSlot":
                    slot.onTryReceiveItem = OnCreditsSlot;
                    break;
                case "QuitSlot":
                    slot.onTryReceiveItem = OnQuitSlot;
                    break;
            }
        }
        
    }

    bool OnEnterBakerySlot(GUIDnDSlot sender)
    {
        OnLoadScene("BakeryMainScene");
        return false;
    }
    bool OnCreditsSlot(GUIDnDSlot sender)
    {
        if (creditsMenu)
        {            
            creditsMenu.SetActive(!creditsMenu.activeSelf);
        }
        return false;
    }
    bool OnQuitSlot(GUIDnDSlot sender)
    {
        Application.Quit();
        return false;
    }

    public void OnLoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
