﻿using UnityEngine;

public class Test_Player : Player
{
    public IngredientListStruct testItems;

    // Start is called before the first frame update
    void Start()
    {
        InitInventory();

        // Add test items and refresh GUI.
        inventory.AddItem(testItems);
        inventoryGUI.RefreshGUI();
    }
}
