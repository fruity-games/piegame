﻿using UnityEngine;
using System.Collections;
using System;

public class Minigame_Mixing : MinigameBase
{
    /** <summary> Number of times player has mixed ingredients. </summary> */
    int numMixes = 0;

    /** <summary> Number of times player has to mix ingredients
     * before minigame is complete. </summary> */
    int numMixesNeeded;

    ///** <summary> Number of times player has to mix ingredients
    // * compared to the difficulty level. </summary> */
    //int[] numMixesDifficulty = new int[] { 2, 5, 10, 25 };

    /** <summary> Image to show bowl with changing sprites as player adds 
     * ingredients. </summary> */
    [Tooltip("Image to show bowl with changing sprites as player adds ingredients.")]
    public GUIImageCycler bowlImage;

    /** <summary> Image to show spoon with changing sprites as player adds 
     * mixes. </summary> */
    [Tooltip("Image to show spoon with changing sprites as player adds mixes.")]
    public GUIImageCycler spoonImage;

    /** <summary> GameObject containing input buttons to be hidden when
     * minigame is started. </summary> */
    [Tooltip("GameObject containing input buttons to be hidden when minigame is started.")]
    public GameObject inputButtonsParent;

    void Start()
    {
        InitMinigame();
        if (itemInputSlot)
        {
            itemInputSlot.onTryReceiveItem = TryReceiveItem;
            itemInputSlot.onItemReceived = OnItemReceived;
        }

        RefreshGUI();
    }

    private void OnItemReceived(IngredientListItemStruct sentItem)
    {
        //if (inventory.inventory.Count > 0
        //    && TryGetItemProcess(sentItem, out var process))
        //{
        //    StartMinigame(process, null);
        //}

        // Remove item from slot (still in inventory).
        if (itemInputSlot)
        {
            itemInputSlot.InitGUI(inventory, null);
        }

        RefreshGUI();
    }

    private void RefreshGUI()
    {
        // Image will not be changed if too many items.
        if (bowlImage)
        {
            bowlImage.CycleImage(inventory.wholeItemCount);
        }


        if (inputButtonsParent)
        {
            // Show when minigame is not being played.
            inputButtonsParent.SetActive(!isActive);
        }
    }

    private bool TryReceiveItem(GUIDnDSlot sender)
    {
        if (sender)
        {
            {
                // Add to player baking log inv. 
                // Ingredient can be returned after it is added to the mixing bowl.
                var pl = FindObjectOfType<Player>();
                if (pl)
                {
                    pl.bakingInventory.AddItem(sender.GetSendingItem());
                }
                    
                return true;
            }
        }
        return false;
    }


    // Minigame overrides

    protected override void OnStarted()
    {
        // Reset values
        numMixes = 0;
        numMixesNeeded = inventory.inventory.Count * 2;
        hasConsumedIngr = true; // Process needs to be saved manually.

        RefreshGUI();
    }

    protected override void OnFinished(bool successful)
    {
        var pl = FindObjectOfType<Player>();
        if (pl)
        {
            if (successful)
            {
                pl.SaveBakingProcess(currentItemProcessData, this);
            }

            pl.bakingInventory.PrintContents();
        }

        // Clear items manually of any extra ingredients.
        inventory.Clear();

        RefreshGUI();
    }

    protected override bool WasSuccessful()
    {
        // Check if the added ingredients are valid (in any quantity).
        var processFound = TryGetItemProcess(inventory, out var process, false);
        if (processFound)
        {
            currentItemProcessData = process;

            // Set quantities to actual quantites used.
            for (int i = 0; i < currentItemProcessData.ingredientsIn.ingredientList.Length; i++)
            {
                var ingr = currentItemProcessData.ingredientsIn.ingredientList[i].ingredientType;
                var newQuant = inventory.inventory[ingr].quantity;
                currentItemProcessData.ingredientsIn.ingredientList[i].quantity = newQuant;
            }
        }
        return processFound && numMixes >= numMixesNeeded;
    }

    protected override bool CanStart(MinigameItemStruct itemProcessData)
    {
        return !isActive && inventory.inventory.Count > 0;
    }


    // GUI input

    public void OnInputConfirm()
    {        
        // Will check before starting minigame.
        StartMinigame(new MinigameItemStruct(), null);
    }

    public void OnInputTrash()
    {
        if (!isActive)
        {
            inventory.Clear();

            RefreshGUI();
        }
    }

    /** <summary> Add current inventory items to player inventory and clear
     * current inventory. </summary> */
     [Obsolete()]
    private void DepositInvToBakingLog()
    {
        // Move from minigame inv to player baking log inv.
        var pl = FindObjectOfType<Player>();
        foreach (var ingrItem in inventory.inventory.Values)
        {

            if (pl)
            {
                pl.bakingInventory.AddItem(ingrItem);
            }

            // Cannot modify while iterating.
            //inventory.RemoveItem(ingrItem.ingredientType);
        }

        inventory.Clear();
        pl.bakingInventory.PrintContents();
    }

    public void AddMix()
    {
        if (isActive)
        {
            numMixes += 1;
            if (spoonImage)
            {
                spoonImage.CycleImage();
            }

            if (numMixes >= numMixesNeeded)
            {
                FinishMinigame();
            }
        }
    }

}
